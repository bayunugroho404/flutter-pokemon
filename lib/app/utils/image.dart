/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

getImageUrl(int id) {
  String url ="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/$id.svg";
  return url;
}