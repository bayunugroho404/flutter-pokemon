import 'package:get/get.dart';

import 'package:pokemon/app/modules/detail/bindings/detail_binding.dart';
import 'package:pokemon/app/modules/detail/views/detail_view.dart';
import 'package:pokemon/app/modules/favorit/bindings/favorit_binding.dart';
import 'package:pokemon/app/modules/favorit/views/favorit_view.dart';
import 'package:pokemon/app/modules/home/bindings/home_binding.dart';
import 'package:pokemon/app/modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL,
      page: () => DetailView(),
      binding: DetailBinding(),
    ),
    GetPage(
      name: _Paths.FAVORIT,
      page: () => FavoritView(),
      binding: FavoritBinding(),
    ),
  ];
}
