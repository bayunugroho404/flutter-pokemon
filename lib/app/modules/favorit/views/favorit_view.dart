import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:pokemon/app/modules/detail/controllers/detail_controller.dart';
import 'package:pokemon/app/utils/size_config.dart';

import '../controllers/favorit_controller.dart';

class FavoritView extends GetView<FavoritController> {

  final controllerDetail = Get.put(DetailController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('FavoritView'),
        centerTitle: true,
      ),
      body: Obx(() => controllerDetail.listFavorit.length > 0
          ? ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        itemCount: controllerDetail.listFavorit.length,
        itemBuilder: (BuildContext context, int index) {
          return Stack(children: <Widget>[
            Positioned(
              top: 20,
              left: SizeConfig.screenWidth / 8,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.all(Radius.circular(20.0)),
                ),
                child: Container(
                  height: SizeConfig.screenHight / 7,
                  width: SizeConfig.screenWidth / 1.3,
                  child: Obx(() => Center(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 40.0),
                        child: Text(
                          controllerDetail.listFavorit[index].name!,
                          style: TextStyle(
                              fontSize:
                              SizeConfig.blockVertical * 3),
                        ),
                      ))),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: SizeConfig.screenHight / 6,
                width: SizeConfig.screenWidth / 3,
                child: SvgPicture.network(
                  controllerDetail.listFavorit[index].image!,
                  semanticsLabel: 'A shark?!',
                  placeholderBuilder: (BuildContext context) =>
                      Container(
                          padding: const EdgeInsets.all(30.0),
                          child: const CircularProgressIndicator()),
                ),
              ),
            ),
            Positioned(
                right: SizeConfig.screenWidth * 0.1,
                top: SizeConfig.screenHight / 14,
                child: IconButton(icon: Icon(
                  Icons.favorite,
                  color: Colors.redAccent,
                  size: SizeConfig.blockVertical * 6,), onPressed: () {
                  controllerDetail.removeFavorit(index);
                },
                ))
          ]);
        },
      )
          : Container()),
    );
  }
}
