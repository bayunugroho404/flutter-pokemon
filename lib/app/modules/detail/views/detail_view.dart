import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pokemon/app/utils/size_config.dart';
import '../controllers/detail_controller.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class DetailView extends GetView<DetailController> {
  String? path;
  String? name;
  String? tag;
  int? id;

  DetailView({this.tag, this.path, this.name, this.id});

  final controller = Get.put(DetailController());

  @override
  Widget build(BuildContext context) {
    controller.getDetailPokemon(id!);
    SizeConfig().init(context);
    controller.setDefaultFavorit();
    return Scaffold(
      appBar: AppBar(
        actions: [
          Obx(()=>IconButton(icon: Icon(Icons.favorite,color: controller.isFavorit.value?Colors.white:Colors.grey,), onPressed: (){
            controller.addFavorit(name!, path!);
            Get.snackbar(
              "Success",
              "Success add to favorit",
            );
          }))
        ],
        title: Text('$name'),
        centerTitle: true,
      ),
      body: Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHight,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/bg.jpg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Stack(
          children: [
            Positioned(
                left: SizeConfig.screenWidth / 13,
                top: 30,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                  child: Container(
                    height: SizeConfig.screenHight / 5,
                    width: SizeConfig.screenWidth / 1.2,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Obx(() => Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Generation : ${controller.generation.value}",
                                  style: TextStyle(
                                      fontSize: SizeConfig.blockVertical * 3),
                                ),
                              )),
                          Obx(() => Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Habitat : ${controller.habitat.value}",
                                  style: TextStyle(
                                      fontSize: SizeConfig.blockVertical * 3),
                                ),
                              )),
                          Obx(() => Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Growthrate : ${controller.growthRate.value}",
                                  style: TextStyle(
                                      fontSize: SizeConfig.blockVertical * 3),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                )),
            Positioned(
                left: SizeConfig.screenWidth * 0.3,
                bottom: SizeConfig.screenHight * 0.3,
                child: IconButton(
                  icon: Icon(
                    Icons.info_outline,
                    size: SizeConfig.blockVertical * 8,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    Get.defaultDialog(
                      title: "Flavor Text Entry",
                      cancelTextColor: Colors.black,
                      textCancel: "       Back       ",
                      content: Obx(() => controller.listDetailPokemon.length > 0
                          ? Expanded(
                              child: ListView.builder(
                                physics: ClampingScrollPhysics(),
                                itemCount: controller.listDetailPokemon.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('${index + 1}. '),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Expanded(
                                          child: Text(controller
                                              .listDetailPokemon[index]
                                              .flavorText!),
                                        ),
                                      ],
                                    ),
                                  ));
                                },
                              ),
                            )
                          : Container()),
                    );
                  },
                )),
            Positioned(
              left: SizeConfig.screenWidth * 0.4,
              bottom: SizeConfig.screenHight * 0.1,
              child: Hero(
                  tag: tag!,
                  child: Container(
                    height: SizeConfig.screenHight / 5,
                    width: SizeConfig.screenWidth / 2,
                    child: SvgPicture.network(
                      path!,
                      semanticsLabel: 'A shark?!',
                      placeholderBuilder: (BuildContext context) => Container(
                          padding: const EdgeInsets.all(30.0),
                          child: const CircularProgressIndicator()),
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
