import 'package:get/get.dart';
import 'package:pokemon/app/data/remote/config/api_config.dart';
import 'package:pokemon/app/data/remote/response/response_get_detail_pokemon.dart';
import 'package:pokemon/app/model/model.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class DetailController extends GetxController {
  final listDetailPokemon = <FlavorTextEntry>[].obs;
  final listFavorit = <Favorit>[].obs;
  final generation =''.obs;
  final isFavorit = false.obs;
  final growthRate =''.obs;
  final habitat =''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getDetailPokemon(int id) {
    listDetailPokemon.clear();
    update();
    APIService().getDetailPokemon(id).then((val){
      listDetailPokemon.value = val.flavorTextEntries!;
      generation.value = val.generation!.name!;
      habitat.value = val.habitat!.name!;
      growthRate.value = val.growthRate!.name!;
      update();
    }).catchError((_){});
  }

  void addFavorit(String name,String image){
    listFavorit.add(new Favorit(
      name:name,
      image: image
    ));
    isFavorit.value = true;
    update();
  }

  void setDefaultFavorit(){
    isFavorit.value = false;
    update();
  }

  void removeFavorit(int position){
    listFavorit.removeAt(position);
    update();
  }

}
