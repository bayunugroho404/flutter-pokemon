import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pokemon/app/modules/detail/views/detail_view.dart';
import 'package:pokemon/app/modules/favorit/views/favorit_view.dart';
import 'package:pokemon/app/utils/image.dart';
import 'package:pokemon/app/utils/size_config.dart';

import '../controllers/home_controller.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(
                Icons.favorite,
                color: Colors.redAccent,
              ),
              onPressed: () {
                Get.to(FavoritView());
              })
        ],
        title: Text('POKEMON'),
        centerTitle: true,
      ),
      body: Obx(() => controller.listPokemon.length > 0
          ? ListView.builder(
              controller: controller.scrollController,
              physics: AlwaysScrollableScrollPhysics(),
              itemCount: controller.listPokemon.length,
              itemBuilder: (BuildContext context, int index) {
                return Hero(
                  tag: "pokemon" + index.toString(),
                  child: Material(
                    child: InkWell(
                      onTap: () {
                        Get.to(DetailView(
                            name: controller.listPokemon[index].name,
                            path: getImageUrl(index + 1),
                            id: index + 1,
                            tag: "pokemon" + index.toString()));
                      },
                      child: Stack(children: <Widget>[
                        Positioned(
                          top: 20,
                          left: SizeConfig.screenWidth / 8,
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20.0)),
                            ),
                            child: Container(
                              height: SizeConfig.screenHight / 7,
                              width: SizeConfig.screenWidth / 1.3,
                              child: Obx(() => Center(
                                      child: Padding(
                                    padding: const EdgeInsets.only(left: 40.0),
                                    child: Text(
                                      controller.listPokemon[index].name!,
                                      style: TextStyle(
                                          fontSize:
                                              SizeConfig.blockVertical * 3),
                                    ),
                                  ))),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            height: SizeConfig.screenHight / 6,
                            width: SizeConfig.screenWidth / 3,
                            child: SvgPicture.network(
                              getImageUrl(index + 1),
                              semanticsLabel: 'A shark?!',
                              placeholderBuilder: (BuildContext context) =>
                                  Container(
                                      padding: const EdgeInsets.all(30.0),
                                      child: const CircularProgressIndicator()),
                            ),
                          ),
                        ),
                        Positioned(
                            right: SizeConfig.screenWidth * 0.1,
                            top: SizeConfig.screenHight / 14,
                            child: Icon(
                              Icons.arrow_forward,
                              size: SizeConfig.blockVertical * 6,
                            ))
                      ]),
                    ),
                  ),
                );
              },
            )
          : Center(child: CircularProgressIndicator())),
    );
  }
}
