import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/app/data/remote/config/api_config.dart';
import 'package:pokemon/app/data/remote/response/response_get_pokemon.dart';
import 'package:pokemon/app/model/model.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */


class HomeController extends GetxController {
  final listPokemon = <Pokemon>[].obs;
  ScrollController scrollController = new ScrollController();
  final index = 1.obs;

  @override
  void onInit() {
    getPokemon(index.value);
    scrollController.addListener(() {
      if(scrollController.position.pixels < scrollController.position.maxScrollExtent){
        index.value + 1;
        getPokemon(index.value);
        update();
      }
    });
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getPokemon(int limit) {
    APIService().getPokemon(limit).then((value){
      value.results?.forEach((element) {
        listPokemon.add(new Pokemon(
            name: element.name
        )) ;
      });
      update();
    }).catchError((_){});
  }
}
