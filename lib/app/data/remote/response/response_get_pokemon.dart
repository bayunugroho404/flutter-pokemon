/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */
import 'dart:convert';

ResponseGetPokemon responseGetPokemonFromJson(String str) => ResponseGetPokemon.fromJson(json.decode(str));

String responseGetPokemonToJson(ResponseGetPokemon data) => json.encode(data.toJson());

class ResponseGetPokemon {
  ResponseGetPokemon({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  String? next;
  String? previous;
  List<Result>? results;

  factory ResponseGetPokemon.fromJson(Map<String, dynamic> json) => ResponseGetPokemon(
    count: json["count"] == null ? null : json["count"],
    next: json["next"] == null ? null : json["next"],
    previous: json["previous"] == null ? null : json["previous"],
    results: json["results"] == null ? null : List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next == null ? null : next,
    "previous": previous == null ? null : previous,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.name,
    this.url,
  });

  String? name;
  String? url;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    name: json["name"] == null ? null : json["name"],
    url: json["url"] == null ? null : json["url"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "url": url == null ? null : url,
  };
}
