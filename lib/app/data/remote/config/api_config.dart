import 'dart:convert';

import 'package:http/http.dart' show Client, Response;
import 'package:pokemon/app/data/remote/response/response_get_detail_pokemon.dart';
import 'package:pokemon/app/data/remote/response/response_get_pokemon.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2021 . All rights reserved.
 */

class APIService {
  Client client = Client();
  String base_url = "https://pokeapi.co/api/v2";

  Future<ResponseGetPokemon> getPokemon(int offset) async {
    Response response;
    response = await client.get(Uri.parse("$base_url/pokemon-species?offset=$offset&limit=5"));
    if (response.statusCode == 200) {
      return ResponseGetPokemon.fromJson(json.decode(response.body));
    } else {
      throw Exception('Error Communication with server');
    }
  }

  Future<ResponseGetDetailPokemon> getDetailPokemon(int id) async {
    Response response;
    response = await client.get(Uri.parse("$base_url/pokemon-species/$id"));
    if (response.statusCode == 200) {
      return ResponseGetDetailPokemon.fromJson(json.decode(response.body));
    } else {
      throw Exception('Error Communication with server');
    }
  }
}
